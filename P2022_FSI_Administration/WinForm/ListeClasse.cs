﻿using P2022_FSI_Administration.Classe;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Collections.Specialized.BitVector32;

namespace P2022_FSI_Administration.WinForm
{
    public partial class ListeClasse : Form
    {
        Utilisateur uti;
        Section cla;
        Section crs;

        public ListeClasse(Section claconnecte, Section crsconnecte, Utilisateur uti)
        {
            InitializeComponent();
            cla = claconnecte;
            crs = crsconnecte;
            this.uti = uti;
        }

        
        private void accueilToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Form formAccueil = new Accueil(uti);
            formAccueil.Show();
            this.Close();
        } 

        /*
        private void ajouterUnClasseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formAjouterClasse = new AjouterClasse();
            formAjouterClasse.Show();
        } */

        private void listeDesClassesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // PanelInterieur.BringToFront();
        }

        private void ListeClasse_Load(object sender, EventArgs e)
        {
            // TODO: cette ligne de code charge les données dans la table 'p2022_Appli_AdministrationDataSet4.classe'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.classeTableAdapter.Fill(this.p2022_Appli_AdministrationDataSet4.classe);
            //mettre la requette ici
        }

        private void bQuitter_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bFermer_Click(object sender, EventArgs e)
        {
            Form formAccueil = new Accueil(uti);
            formAccueil.Show();
            this.Close();
        }

        private void dgvListeClasse_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void accueilToolStripMenuItem2_Click_1(object sender, EventArgs e)
        {
            Form formAccueil = new Accueil(uti);
            formAccueil.Show();
            this.Close();
        }

        private void listeDesEtudiantsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formListeEtudiant = new ListeEtudiant(uti);
            formListeEtudiant.Show();
            this.Close();
        }

        private void ajouterUnEtudiantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formAjouterEtudiant = new AjouterEtudiant();
            formAjouterEtudiant.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void gestionClasseToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void gestionEtudiantToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void gestionCoursToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listeDesCoursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            Form formListeCours = new ListeCours(crs);
            formListeCours.Show();
        }
    }
}
