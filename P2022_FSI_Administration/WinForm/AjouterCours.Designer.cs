﻿
namespace P2022_FSI_Administration.WinForm
{
    partial class AjouterCours
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tbNom = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.bRetour = new System.Windows.Forms.Button();
            this.bEnregistrer = new System.Windows.Forms.Button();
            this.bEffacer = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tbDes = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbClasse = new System.Windows.Forms.ComboBox();
            this.classeBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.p2022_Appli_AdministrationDataSet1 = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet1();
            this.label4 = new System.Windows.Forms.Label();
            this.classeTableAdapter = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet1TableAdapters.classeTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.classeBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // tbNom
            // 
            this.tbNom.Location = new System.Drawing.Point(451, 175);
            this.tbNom.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbNom.Name = "tbNom";
            this.tbNom.Size = new System.Drawing.Size(164, 26);
            this.tbNom.TabIndex = 21;
            this.tbNom.TextChanged += new System.EventHandler(this.tbNom_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.8F);
            this.label1.Location = new System.Drawing.Point(240, 164);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(219, 37);
            this.label1.TabIndex = 20;
            this.label1.Text = "Nom du cours";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // bRetour
            // 
            this.bRetour.BackColor = System.Drawing.Color.LightCyan;
            this.bRetour.Location = new System.Drawing.Point(362, 449);
            this.bRetour.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.bRetour.Name = "bRetour";
            this.bRetour.Size = new System.Drawing.Size(179, 50);
            this.bRetour.TabIndex = 19;
            this.bRetour.Text = "RETOUR";
            this.bRetour.UseVisualStyleBackColor = false;
            this.bRetour.Click += new System.EventHandler(this.bRetour_Click);
            // 
            // bEnregistrer
            // 
            this.bEnregistrer.BackColor = System.Drawing.Color.LightCyan;
            this.bEnregistrer.Location = new System.Drawing.Point(451, 391);
            this.bEnregistrer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.bEnregistrer.Name = "bEnregistrer";
            this.bEnregistrer.Size = new System.Drawing.Size(184, 50);
            this.bEnregistrer.TabIndex = 17;
            this.bEnregistrer.Text = "ENREGISTRER";
            this.bEnregistrer.UseVisualStyleBackColor = false;
            this.bEnregistrer.Click += new System.EventHandler(this.bEnregistrer_Click);
            // 
            // bEffacer
            // 
            this.bEffacer.BackColor = System.Drawing.Color.LightCyan;
            this.bEffacer.Location = new System.Drawing.Point(266, 391);
            this.bEffacer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.bEffacer.Name = "bEffacer";
            this.bEffacer.Size = new System.Drawing.Size(179, 50);
            this.bEffacer.TabIndex = 16;
            this.bEffacer.Text = "EFFACER";
            this.bEffacer.UseVisualStyleBackColor = false;
            this.bEffacer.Click += new System.EventHandler(this.bEffacer_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.8F);
            this.label2.Location = new System.Drawing.Point(240, 221);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(310, 37);
            this.label2.TabIndex = 22;
            this.label2.Text = "Description du cours";
            // 
            // tbDes
            // 
            this.tbDes.Location = new System.Drawing.Point(541, 232);
            this.tbDes.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbDes.Name = "tbDes";
            this.tbDes.Size = new System.Drawing.Size(164, 26);
            this.tbDes.TabIndex = 23;
            this.tbDes.TextChanged += new System.EventHandler(this.tbDes_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.8F);
            this.label3.Location = new System.Drawing.Point(240, 284);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(326, 37);
            this.label3.TabIndex = 24;
            this.label3.Text = "Sélectionner la classe";
            // 
            // cbClasse
            // 
            this.cbClasse.DataSource = this.classeBindingSource;
            this.cbClasse.DisplayMember = "libelleclasse";
            this.cbClasse.FormattingEnabled = true;
            this.cbClasse.Location = new System.Drawing.Point(559, 295);
            this.cbClasse.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbClasse.Name = "cbClasse";
            this.cbClasse.Size = new System.Drawing.Size(213, 28);
            this.cbClasse.TabIndex = 25;
            this.cbClasse.ValueMember = "idclasse";
            this.cbClasse.SelectedIndexChanged += new System.EventHandler(this.cbClasse_SelectedIndexChanged);
            // 
            // classeBindingSource
            // 
            this.classeBindingSource.DataMember = "classe";
            this.classeBindingSource.DataSource = this.p2022_Appli_AdministrationDataSet1;
            // 
            // p2022_Appli_AdministrationDataSet1
            // 
            this.p2022_Appli_AdministrationDataSet1.DataSetName = "P2022_Appli_AdministrationDataSet1";
            this.p2022_Appli_AdministrationDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.8F);
            this.label4.Location = new System.Drawing.Point(336, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(252, 37);
            this.label4.TabIndex = 26;
            this.label4.Text = "Ajouter un cours";
            // 
            // classeTableAdapter
            // 
            this.classeTableAdapter.ClearBeforeFill = true;
            // 
            // AjouterCours
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 562);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbClasse);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbDes);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbNom);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bRetour);
            this.Controls.Add(this.bEnregistrer);
            this.Controls.Add(this.bEffacer);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "AjouterCours";
            this.Text = "AjouterCours";
            this.Load += new System.EventHandler(this.AjouterCours_Load);
            ((System.ComponentModel.ISupportInitialize)(this.classeBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbNom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bRetour;
        private System.Windows.Forms.Button bEnregistrer;
        private System.Windows.Forms.Button bEffacer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbDes;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbClasse;
        private System.Windows.Forms.Label label4;
        private P2022_Appli_AdministrationDataSet1 p2022_Appli_AdministrationDataSet1;
        private System.Windows.Forms.BindingSource classeBindingSource;
        private P2022_Appli_AdministrationDataSet1TableAdapters.classeTableAdapter classeTableAdapter;
    }
}