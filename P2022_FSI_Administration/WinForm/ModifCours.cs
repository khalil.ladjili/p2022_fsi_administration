﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace P2022_FSI_Administration.WinForm
{
    public partial class ModifCours : Form

    {
        NpgsqlConnection maConnexion;
        NpgsqlCommand commande;
        public ModifCours()
        {
            InitializeComponent();
        }
        private void ModifCours_Load(object sender, EventArgs e)
        {
            // TODO: cette ligne de code charge les données dans la table 'p2022_Appli_AdministrationDataSet1.classe'. Vous pouvez la déplacer ou la supprimer selon les besoins.
           //  this.classeTableAdapter.Fill(this.p2022_Appli_AdministrationDataSet1.classe);
            // TODO: cette ligne de code charge les données dans la table 'p2022_Appli_AdministrationDataSet3.cours'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            // this.coursTableAdapter.Fill(this.p2022_Appli_AdministrationDataSet3.cours);

        }

        private void bEffacer_Click(object sender, EventArgs e)
        {
            reInitialisation();

        }

        private void bEnregistrer_Click_1(object sender, EventArgs e)
        {
            // Ajout des variables
            // int idCours = cbIdCours.SelectedIndex;
            // string nomCours = cbNomCours.Text;
            // string infoCours = cbInfCours.Text;
            // string libClasse = cbLibClasse.Text;
            try
            {
                string connexion = "Server=localhost;Port=5432;Database=P2022_Appli_Administration;User Id=postgres;Password=2bn2XW2S;";
                maConnexion = new NpgsqlConnection(connexion);
                maConnexion.Open();
                //Modification des cours
                string update = "UPDATE cours, classe set cours.libellecours = :nomCours , cours.descriptioncours= :infoCours, classe.libelleclasse= :libClasse WHERE cours.idcours = :idCours  " +
                    "AND cours.idcours= classe.idcours;";
                commande = new NpgsqlCommand(update, maConnexion);

                // ajout des paramêtres des infos
                /*commande.Parameters.Add(new NpgsqlParameter("idCours", NpgsqlDbType.Integer)).Value = idCours;
                commande.Parameters.Add(new NpgsqlParameter("nomCours", NpgsqlDbType.Varchar)).Value = nomCours;
                commande.Parameters.Add(new NpgsqlParameter("infoCours", NpgsqlDbType.Varchar)).Value = infoCours;
                commande.Parameters.Add(new NpgsqlParameter("libClasse", NpgsqlDbType.Varchar)).Value = libClasse;*/
                commande.Prepare();
                commande.CommandType = CommandType.Text;
                commande.ExecuteNonQuery();
                MessageBox.Show("Cours Modifier");
                reInitialisation();

            }
            finally
            {
                if (commande != null) commande.Dispose();
                if (maConnexion != null) maConnexion.Close();

            }
        }
        private void reInitialisation()
        {
            // cbIdCours.SelectedIndex = 0;
            // cbNomCours.Text = "";
            // cbInfCours.Text = "";
            // cbLibClasse.Text = "";
        }
        private void bRetour_Click_1(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}