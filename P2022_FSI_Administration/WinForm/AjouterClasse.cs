﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace P2022_FSI_Administration.WinForm
{
    public partial class AjouterClasse : Form
    {
        NpgsqlConnection maConnexion;
        NpgsqlCommand commande;
        public AjouterClasse()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void bEnregistrer_Click(object sender, EventArgs e)
        {
            //Enregistrer en BDD
            string classe = textBox1.Text;

            try
            {
                string connexion = "Server=localhost;Port=5432;Database=P2022_Appli_Administration;User Id=postgres;Password=2bn2XW2S;";
                maConnexion = new NpgsqlConnection(connexion);
                maConnexion = new NpgsqlConnection(connexion);
                maConnexion.Open();
                string insert = "INSERT INTO classe (libelleClasse) values ( :libelleClasse);";
                commande = new NpgsqlCommand(insert, maConnexion);
                commande.Parameters.Add(new NpgsqlParameter("libelleClasse", NpgsqlDbType.Varchar)).Value = classe;
                commande.Prepare();
                commande.CommandType = CommandType.Text;
                commande.ExecuteNonQuery();
                MessageBox.Show("Classe ajouté");
                reInitialisation();

            }
            finally
            {
                if (commande != null) commande.Dispose();
                if (maConnexion != null) maConnexion.Close();

            }

        }

        private void reInitialisation()
        {
            textBox1.Text = "";
        }

        private void AjouterClasse_Load(object sender, EventArgs e)
        {
            // TODO: cette ligne de code charge les données dans la table 'p2022_Appli_AdministrationDataSet1.classe'. Vous pouvez la déplacer ou la supprimer selon les besoins.
                       // this.classeTableAdapter.Fill(this.p2022_Appli_AdministrationDataSet.classe);

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void bRetour_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bEffacer_Click_1(object sender, EventArgs e)
        {
            reInitialisation();
        }
    }
}
