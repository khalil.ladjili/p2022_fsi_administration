﻿
namespace P2022_FSI_Administration.WinForm
{
    partial class AjouterClasse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bRetour = new System.Windows.Forms.Button();
            this.bEnregistrer = new System.Windows.Forms.Button();
            this.bEffacer = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // bRetour
            // 
            this.bRetour.BackColor = System.Drawing.Color.LightCyan;
            this.bRetour.Location = new System.Drawing.Point(109, 399);
            this.bRetour.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.bRetour.Name = "bRetour";
            this.bRetour.Size = new System.Drawing.Size(179, 50);
            this.bRetour.TabIndex = 13;
            this.bRetour.Text = "RETOUR";
            this.bRetour.UseVisualStyleBackColor = false;
            this.bRetour.Click += new System.EventHandler(this.bRetour_Click_1);
            // 
            // bEnregistrer
            // 
            this.bEnregistrer.BackColor = System.Drawing.Color.LightCyan;
            this.bEnregistrer.Location = new System.Drawing.Point(198, 341);
            this.bEnregistrer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.bEnregistrer.Name = "bEnregistrer";
            this.bEnregistrer.Size = new System.Drawing.Size(184, 50);
            this.bEnregistrer.TabIndex = 11;
            this.bEnregistrer.Text = "ENREGISTRER";
            this.bEnregistrer.UseVisualStyleBackColor = false;
            this.bEnregistrer.Click += new System.EventHandler(this.bEnregistrer_Click);
            // 
            // bEffacer
            // 
            this.bEffacer.BackColor = System.Drawing.Color.LightCyan;
            this.bEffacer.Location = new System.Drawing.Point(12, 341);
            this.bEffacer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.bEffacer.Name = "bEffacer";
            this.bEffacer.Size = new System.Drawing.Size(179, 50);
            this.bEffacer.TabIndex = 10;
            this.bEffacer.Text = "EFFACER";
            this.bEffacer.UseVisualStyleBackColor = false;
            this.bEffacer.Click += new System.EventHandler(this.bEffacer_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.8F);
            this.label1.Location = new System.Drawing.Point(30, 194);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 37);
            this.label1.TabIndex = 14;
            this.label1.Text = "Classe";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(177, 194);
            this.textBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(164, 26);
            this.textBox1.TabIndex = 15;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // AjouterClasse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 496);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bRetour);
            this.Controls.Add(this.bEnregistrer);
            this.Controls.Add(this.bEffacer);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "AjouterClasse";
            this.Text = "AjouterClasse";
            this.Load += new System.EventHandler(this.AjouterClasse_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bRetour;
        private System.Windows.Forms.Button bEnregistrer;
        private System.Windows.Forms.Button bEffacer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
    }
}