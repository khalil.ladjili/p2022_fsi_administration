﻿
namespace P2022_FSI_Administration.WinForm
{
    partial class ModifEtudiant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbEtudiant = new System.Windows.Forms.ComboBox();
            this.tbAENom = new System.Windows.Forms.TextBox();
            this.tbAEPrenom = new System.Windows.Forms.TextBox();
            this.tbMail = new System.Windows.Forms.TextBox();
            this.tbTelephone = new System.Windows.Forms.TextBox();
            this.bRetour = new System.Windows.Forms.Button();
            this.bEffacer = new System.Windows.Forms.Button();
            this.bEnregistrer = new System.Windows.Forms.Button();
            this.tbClasse = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cbEtudiant
            // 
            this.cbEtudiant.DisplayMember = "libelleclasse";
            this.cbEtudiant.FormattingEnabled = true;
            this.cbEtudiant.Location = new System.Drawing.Point(423, 97);
            this.cbEtudiant.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbEtudiant.Name = "cbEtudiant";
            this.cbEtudiant.Size = new System.Drawing.Size(213, 28);
            this.cbEtudiant.TabIndex = 26;
            this.cbEtudiant.ValueMember = "idclasse";
            // 
            // tbAENom
            // 
            this.tbAENom.Location = new System.Drawing.Point(423, 163);
            this.tbAENom.Name = "tbAENom";
            this.tbAENom.Size = new System.Drawing.Size(190, 26);
            this.tbAENom.TabIndex = 27;
            // 
            // tbAEPrenom
            // 
            this.tbAEPrenom.Location = new System.Drawing.Point(423, 235);
            this.tbAEPrenom.Name = "tbAEPrenom";
            this.tbAEPrenom.Size = new System.Drawing.Size(190, 26);
            this.tbAEPrenom.TabIndex = 28;
            // 
            // tbMail
            // 
            this.tbMail.Location = new System.Drawing.Point(423, 378);
            this.tbMail.Multiline = true;
            this.tbMail.Name = "tbMail";
            this.tbMail.Size = new System.Drawing.Size(253, 53);
            this.tbMail.TabIndex = 29;
            // 
            // tbTelephone
            // 
            this.tbTelephone.Location = new System.Drawing.Point(423, 465);
            this.tbTelephone.Name = "tbTelephone";
            this.tbTelephone.Size = new System.Drawing.Size(190, 26);
            this.tbTelephone.TabIndex = 30;
            // 
            // bRetour
            // 
            this.bRetour.BackColor = System.Drawing.Color.LightCyan;
            this.bRetour.Location = new System.Drawing.Point(934, 127);
            this.bRetour.Name = "bRetour";
            this.bRetour.Size = new System.Drawing.Size(159, 40);
            this.bRetour.TabIndex = 32;
            this.bRetour.Text = "RETOUR";
            this.bRetour.UseVisualStyleBackColor = false;
            // 
            // bEffacer
            // 
            this.bEffacer.BackColor = System.Drawing.Color.LightCyan;
            this.bEffacer.Location = new System.Drawing.Point(934, 75);
            this.bEffacer.Name = "bEffacer";
            this.bEffacer.Size = new System.Drawing.Size(159, 40);
            this.bEffacer.TabIndex = 33;
            this.bEffacer.Text = "EFFACER";
            this.bEffacer.UseVisualStyleBackColor = false;
            // 
            // bEnregistrer
            // 
            this.bEnregistrer.BackColor = System.Drawing.Color.LightCyan;
            this.bEnregistrer.Location = new System.Drawing.Point(929, 29);
            this.bEnregistrer.Name = "bEnregistrer";
            this.bEnregistrer.Size = new System.Drawing.Size(164, 40);
            this.bEnregistrer.TabIndex = 34;
            this.bEnregistrer.Text = "ENREGISTRER";
            this.bEnregistrer.UseVisualStyleBackColor = false;
            // 
            // tbClasse
            // 
            this.tbClasse.DisplayMember = "libelleclasse";
            this.tbClasse.FormattingEnabled = true;
            this.tbClasse.Location = new System.Drawing.Point(423, 304);
            this.tbClasse.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbClasse.Name = "tbClasse";
            this.tbClasse.Size = new System.Drawing.Size(213, 28);
            this.tbClasse.TabIndex = 35;
            this.tbClasse.ValueMember = "idclasse";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(247, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 20);
            this.label1.TabIndex = 36;
            this.label1.Text = "Modifier un étudiant";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(251, 307);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 20);
            this.label2.TabIndex = 37;
            this.label2.Text = "La classe";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(251, 235);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 20);
            this.label3.TabIndex = 38;
            this.label3.Text = "Prenom";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(251, 166);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 20);
            this.label4.TabIndex = 39;
            this.label4.Text = "Nom";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(251, 97);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 20);
            this.label5.TabIndex = 40;
            this.label5.Text = "Numero etudiant";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(251, 468);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 20);
            this.label6.TabIndex = 41;
            this.label6.Text = "Tel";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(251, 394);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 20);
            this.label7.TabIndex = 42;
            this.label7.Text = "Email";
            // 
            // ModifEtudiant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1105, 562);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbClasse);
            this.Controls.Add(this.bEnregistrer);
            this.Controls.Add(this.bEffacer);
            this.Controls.Add(this.bRetour);
            this.Controls.Add(this.tbTelephone);
            this.Controls.Add(this.tbMail);
            this.Controls.Add(this.tbAEPrenom);
            this.Controls.Add(this.tbAENom);
            this.Controls.Add(this.cbEtudiant);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ModifEtudiant";
            this.Text = "ModifEtudiant";
            this.Load += new System.EventHandler(this.ModifEtudiant_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cbEtudiant;
        private System.Windows.Forms.TextBox tbAENom;
        private System.Windows.Forms.TextBox tbAEPrenom;
        private System.Windows.Forms.TextBox tbMail;
        private System.Windows.Forms.TextBox tbTelephone;
        private System.Windows.Forms.Button bRetour;
        private System.Windows.Forms.Button bEffacer;
        private System.Windows.Forms.Button bEnregistrer;
        private System.Windows.Forms.ComboBox tbClasse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}