﻿using P2022_FSI_Administration.Classe;
using P2022_FSI_Administration.WinForm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Collections.Specialized.BitVector32;

namespace P2022_FSI_Administration
{
    public partial class Accueil : Form
    {
        Utilisateur uti;
        Section cla;
        Section crs;

        public Accueil(Utilisateur uticonnecte)
        {
            InitializeComponent();
            uti = uticonnecte;
            Form formConnexion = new Connexion();
            formConnexion.Close();
            //tbUserConnecte.Text = uti.LoginUtilisateur;
        }

        
        private void accueilToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form formAccueil = new Accueil(uti);
            formAccueil.Show();
            //this.Close();
        }


        private void accueilToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Form formAccueil = new Accueil(uti);
            formAccueil.Show();
            this.Close();
            
            //PanelInterieur.BringToFront();
        }

        private void listeDesEtudiantsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formListeEtudiant = new ListeEtudiant(uti);
            formListeEtudiant.Show();
            this.Close();
        }

        private void ajouterUnEtudiantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formAjouterEtudiant = new AjouterEtudiant();
            formAjouterEtudiant.Show();
        }

        
        private void bQuitter_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void listeDesClassesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formListeClasses = new ListeClasse(cla, crs, uti);
            formListeClasses.Show();
            this.Close();
        }

        private void ajouterUneClasseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formAjouterClasse = new AjouterClasse();
            formAjouterClasse.Show();
        }

        private void PanelInterieur_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void gestionCoursToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void listeDesCoursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new ListeCours(crs);
            f.Show();
            this.Close();
        }

        private void Accueil_Load(object sender, EventArgs e)
        {

        }

        private void ajouterUnCoursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formAjouterCours = new AjouterCours();
            formAjouterCours.Show();
        }

        private void gestionEtudiantToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void gestionClasseToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
