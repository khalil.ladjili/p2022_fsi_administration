﻿using P2022_FSI_Administration.Classe;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Collections.Specialized.BitVector32;

namespace P2022_FSI_Administration.WinForm
{
    public partial class ListeCours : Form
    {
        Utilisateur uti;
        Section crs;
        Section cla;
        public ListeCours(Section crsconnecte) 
        {
            InitializeComponent();
            crs = crsconnecte;

        }
        private void accueilToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Form formAccueil = new Accueil(uti);
            formAccueil.Show();
            this.Close();
        }

        /*
        private void listeDesCoursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // PanelInterieur.BringToFront();
        } */

        private void ajoutCoursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formAjouterCours = new AjouterCours();
            formAjouterCours.Show();
        }

        private void listeCoursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new ListeCours(crs);
            f.Show();
            this.Close();
        }

        private void ListeCours_Load(object sender, EventArgs e)
        {
            // TODO: cette ligne de code charge les données dans la table 'p2022_Appli_AdministrationDataSet13.cours'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.coursTableAdapter1.Fill(this.p2022_Appli_AdministrationDataSet13.cours);
            // TODO: cette ligne de code charge les données dans la table 'p2022_Appli_AdministrationDataSet5.cours'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.coursTableAdapter.Fill(this.p2022_Appli_AdministrationDataSet5.cours);

        }

        private void bQuitter_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bFermer_Click(object sender, EventArgs e)
        {
            Form formAccueil = new Accueil(uti);
            formAccueil.Show();
            this.Close();
        }

        //private void dgvListeCours_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    var cells = dgvListeCours.Rows[e.RowIndex].Cells; // récupère l'objet de la cellule cliquée
        //    var id = (int)cells[0].Value;   // l'index de la cellule 
        //    var nom = (string)cells[1].Value;   // première valeur contenue dans la cellule
        //    var description = (string)cells[2].Value;
        //    var idclasse = (int)cells[3].Value;

        //    new ModifierCours(id, nom, description, idclasse)
        //        .Show();
        //}

        private void dgvListeCours_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
 
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void accueilToolStripMenuItem2_Click_1(object sender, EventArgs e)
        {
            Form formAccueil = new Accueil(uti);
            formAccueil.Show();
            this.Close();
        }

        private void listeDesEtudiantsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formListeEtudiant = new ListeEtudiant(uti);
            formListeEtudiant.Show();
            this.Close();
        }

        private void ajouterUnEtudiantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formAjouterEtudiant = new AjouterEtudiant();
            formAjouterEtudiant.Show();
        }

        private void listeDesClassesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formListeClasses = new ListeClasse(crs, cla, uti);
            formListeClasses.Show();
            this.Close();
        }

        private void ajouterUneClasseToolStripMenuItem_Click(object sender, EventArgs e)
        {
             Form formAjouterClasse = new AjouterClasse();
             formAjouterClasse.Show();
     
        }

        private void gestionClasseToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
