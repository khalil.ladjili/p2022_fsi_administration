﻿
namespace P2022_FSI_Administration
{
    partial class ListeEtudiant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PanelQuitter = new System.Windows.Forms.Panel();
            this.bQuitter = new System.Windows.Forms.Button();
            this.PanelMenu = new System.Windows.Forms.Panel();
            this.msGlobal = new System.Windows.Forms.MenuStrip();
            this.accueilToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionEtudiantToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesEtudiantsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajouterUnEtudiantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionClasseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesClassesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajouterUneClasseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionCoursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesCoursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajouterUnCoursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PanelInterieur = new System.Windows.Forms.Panel();
            this.bFermer = new System.Windows.Forms.Button();
            this.dgvListeEtudiant = new System.Windows.Forms.DataGridView();
            this.etudiantBindingSource5 = new System.Windows.Forms.BindingSource(this.components);
            this.p2022_Appli_AdministrationDataSet12 = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet12();
            this.etudiantBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.p2022_Appli_AdministrationDataSet8 = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet8();
            this.etudiantBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.p2022_Appli_AdministrationDataSet6 = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet6();
            this.etudiantBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.p2022_Appli_AdministrationDataSet = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet();
            this.etudiantTableAdapter = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSetTableAdapters.etudiantTableAdapter();
            this.etudiantTableAdapter1 = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet6TableAdapters.etudiantTableAdapter();
            this.etudiantTableAdapter2 = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet8TableAdapters.etudiantTableAdapter();
            this.p2022_Appli_AdministrationDataSet10 = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet10();
            this.etudiantBindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.etudiantTableAdapter3 = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet10TableAdapters.etudiantTableAdapter();
            this.etudiantBindingSource4 = new System.Windows.Forms.BindingSource(this.components);
            this.p2022_Appli_AdministrationDataSet11 = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet11();
            this.etudiantTableAdapter4 = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet11TableAdapters.etudiantTableAdapter();
            this.etudiantTableAdapter5 = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet12TableAdapters.etudiantTableAdapter();
            this.nometudiant = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prenometudiantDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idclasseDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mailetudiant = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teletudiant = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PanelQuitter.SuspendLayout();
            this.PanelMenu.SuspendLayout();
            this.msGlobal.SuspendLayout();
            this.PanelInterieur.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListeEtudiant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet11)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelQuitter
            // 
            this.PanelQuitter.Controls.Add(this.bQuitter);
            this.PanelQuitter.Location = new System.Drawing.Point(899, 0);
            this.PanelQuitter.Name = "PanelQuitter";
            this.PanelQuitter.Size = new System.Drawing.Size(123, 46);
            this.PanelQuitter.TabIndex = 11;
            // 
            // bQuitter
            // 
            this.bQuitter.BackColor = System.Drawing.Color.LightCyan;
            this.bQuitter.Location = new System.Drawing.Point(4, 3);
            this.bQuitter.Name = "bQuitter";
            this.bQuitter.Size = new System.Drawing.Size(115, 40);
            this.bQuitter.TabIndex = 7;
            this.bQuitter.Text = "QUITTER";
            this.bQuitter.UseVisualStyleBackColor = false;
            this.bQuitter.Click += new System.EventHandler(this.bQuitter_Click);
            // 
            // PanelMenu
            // 
            this.PanelMenu.Controls.Add(this.msGlobal);
            this.PanelMenu.Location = new System.Drawing.Point(87, 0);
            this.PanelMenu.Name = "PanelMenu";
            this.PanelMenu.Size = new System.Drawing.Size(806, 29);
            this.PanelMenu.TabIndex = 12;
            // 
            // msGlobal
            // 
            this.msGlobal.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.msGlobal.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.msGlobal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accueilToolStripMenuItem2,
            this.gestionEtudiantToolStripMenuItem1,
            this.gestionClasseToolStripMenuItem,
            this.gestionCoursToolStripMenuItem});
            this.msGlobal.Location = new System.Drawing.Point(0, 0);
            this.msGlobal.Name = "msGlobal";
            this.msGlobal.Size = new System.Drawing.Size(806, 33);
            this.msGlobal.TabIndex = 0;
            this.msGlobal.Text = "Menu";
            // 
            // accueilToolStripMenuItem2
            // 
            this.accueilToolStripMenuItem2.Name = "accueilToolStripMenuItem2";
            this.accueilToolStripMenuItem2.Size = new System.Drawing.Size(83, 29);
            this.accueilToolStripMenuItem2.Text = "Accueil";
            this.accueilToolStripMenuItem2.Click += new System.EventHandler(this.accueilToolStripMenuItem2_Click);
            // 
            // gestionEtudiantToolStripMenuItem1
            // 
            this.gestionEtudiantToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listeDesEtudiantsToolStripMenuItem,
            this.ajouterUnEtudiantToolStripMenuItem});
            this.gestionEtudiantToolStripMenuItem1.Name = "gestionEtudiantToolStripMenuItem1";
            this.gestionEtudiantToolStripMenuItem1.Size = new System.Drawing.Size(158, 29);
            this.gestionEtudiantToolStripMenuItem1.Text = "Gestion Etudiant";
            this.gestionEtudiantToolStripMenuItem1.Click += new System.EventHandler(this.gestionEtudiantToolStripMenuItem1_Click);
            // 
            // listeDesEtudiantsToolStripMenuItem
            // 
            this.listeDesEtudiantsToolStripMenuItem.Name = "listeDesEtudiantsToolStripMenuItem";
            this.listeDesEtudiantsToolStripMenuItem.Size = new System.Drawing.Size(267, 34);
            this.listeDesEtudiantsToolStripMenuItem.Text = "Liste des étudiants";
            this.listeDesEtudiantsToolStripMenuItem.Click += new System.EventHandler(this.listeDesEtudiantsToolStripMenuItem_Click);
            // 
            // ajouterUnEtudiantToolStripMenuItem
            // 
            this.ajouterUnEtudiantToolStripMenuItem.Name = "ajouterUnEtudiantToolStripMenuItem";
            this.ajouterUnEtudiantToolStripMenuItem.Size = new System.Drawing.Size(267, 34);
            this.ajouterUnEtudiantToolStripMenuItem.Text = "Ajouter un étudiant";
            this.ajouterUnEtudiantToolStripMenuItem.Click += new System.EventHandler(this.ajouterUnEtudiantToolStripMenuItem_Click);
            // 
            // gestionClasseToolStripMenuItem
            // 
            this.gestionClasseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listeDesClassesToolStripMenuItem,
            this.ajouterUneClasseToolStripMenuItem});
            this.gestionClasseToolStripMenuItem.Name = "gestionClasseToolStripMenuItem";
            this.gestionClasseToolStripMenuItem.Size = new System.Drawing.Size(139, 29);
            this.gestionClasseToolStripMenuItem.Text = "Gestion classe";
            this.gestionClasseToolStripMenuItem.Click += new System.EventHandler(this.gestionClasseToolStripMenuItem_Click);
            // 
            // listeDesClassesToolStripMenuItem
            // 
            this.listeDesClassesToolStripMenuItem.Name = "listeDesClassesToolStripMenuItem";
            this.listeDesClassesToolStripMenuItem.Size = new System.Drawing.Size(257, 34);
            this.listeDesClassesToolStripMenuItem.Text = "Liste des classes";
            this.listeDesClassesToolStripMenuItem.Click += new System.EventHandler(this.listeDesClassesToolStripMenuItem_Click);
            // 
            // ajouterUneClasseToolStripMenuItem
            // 
            this.ajouterUneClasseToolStripMenuItem.Name = "ajouterUneClasseToolStripMenuItem";
            this.ajouterUneClasseToolStripMenuItem.Size = new System.Drawing.Size(257, 34);
            this.ajouterUneClasseToolStripMenuItem.Text = "Ajouter une classe";
            // 
            // gestionCoursToolStripMenuItem
            // 
            this.gestionCoursToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listeDesCoursToolStripMenuItem,
            this.ajouterUnCoursToolStripMenuItem});
            this.gestionCoursToolStripMenuItem.Name = "gestionCoursToolStripMenuItem";
            this.gestionCoursToolStripMenuItem.Size = new System.Drawing.Size(139, 29);
            this.gestionCoursToolStripMenuItem.Text = "Gestion Cours";
            this.gestionCoursToolStripMenuItem.Click += new System.EventHandler(this.gestionCoursToolStripMenuItem_Click);
            // 
            // listeDesCoursToolStripMenuItem
            // 
            this.listeDesCoursToolStripMenuItem.Name = "listeDesCoursToolStripMenuItem";
            this.listeDesCoursToolStripMenuItem.Size = new System.Drawing.Size(250, 34);
            this.listeDesCoursToolStripMenuItem.Text = "Liste des cours";
            // 
            // ajouterUnCoursToolStripMenuItem
            // 
            this.ajouterUnCoursToolStripMenuItem.Name = "ajouterUnCoursToolStripMenuItem";
            this.ajouterUnCoursToolStripMenuItem.Size = new System.Drawing.Size(250, 34);
            this.ajouterUnCoursToolStripMenuItem.Text = "Ajouter un cours ";
            // 
            // PanelInterieur
            // 
            this.PanelInterieur.Controls.Add(this.dgvListeEtudiant);
            this.PanelInterieur.Location = new System.Drawing.Point(87, 35);
            this.PanelInterieur.Name = "PanelInterieur";
            this.PanelInterieur.Size = new System.Drawing.Size(806, 482);
            this.PanelInterieur.TabIndex = 12;
            // 
            // bFermer
            // 
            this.bFermer.BackColor = System.Drawing.Color.LightCyan;
            this.bFermer.Location = new System.Drawing.Point(899, 52);
            this.bFermer.Name = "bFermer";
            this.bFermer.Size = new System.Drawing.Size(115, 40);
            this.bFermer.TabIndex = 13;
            this.bFermer.Text = "FERMER";
            this.bFermer.UseVisualStyleBackColor = false;
            this.bFermer.Click += new System.EventHandler(this.bFermer_Click);
            // 
            // dgvListeEtudiant
            // 
            this.dgvListeEtudiant.AllowUserToOrderColumns = true;
            this.dgvListeEtudiant.AutoGenerateColumns = false;
            this.dgvListeEtudiant.BackgroundColor = System.Drawing.Color.White;
            this.dgvListeEtudiant.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListeEtudiant.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nometudiant,
            this.prenometudiantDataGridViewTextBoxColumn,
            this.idclasseDataGridViewTextBoxColumn,
            this.mailetudiant,
            this.teletudiant});
            this.dgvListeEtudiant.DataSource = this.etudiantBindingSource5;
            this.dgvListeEtudiant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvListeEtudiant.GridColor = System.Drawing.Color.SkyBlue;
            this.dgvListeEtudiant.Location = new System.Drawing.Point(0, 0);
            this.dgvListeEtudiant.Name = "dgvListeEtudiant";
            this.dgvListeEtudiant.RowHeadersWidth = 51;
            this.dgvListeEtudiant.Size = new System.Drawing.Size(806, 482);
            this.dgvListeEtudiant.TabIndex = 0;
            this.dgvListeEtudiant.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvListeEtudiant_CellContentClick);
            // 
            // etudiantBindingSource5
            // 
            this.etudiantBindingSource5.DataMember = "etudiant";
            this.etudiantBindingSource5.DataSource = this.p2022_Appli_AdministrationDataSet12;
            // 
            // p2022_Appli_AdministrationDataSet12
            // 
            this.p2022_Appli_AdministrationDataSet12.DataSetName = "P2022_Appli_AdministrationDataSet12";
            this.p2022_Appli_AdministrationDataSet12.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // etudiantBindingSource2
            // 
            this.etudiantBindingSource2.DataMember = "etudiant";
            this.etudiantBindingSource2.DataSource = this.p2022_Appli_AdministrationDataSet8;
            // 
            // p2022_Appli_AdministrationDataSet8
            // 
            this.p2022_Appli_AdministrationDataSet8.DataSetName = "P2022_Appli_AdministrationDataSet8";
            this.p2022_Appli_AdministrationDataSet8.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // etudiantBindingSource1
            // 
            this.etudiantBindingSource1.DataMember = "etudiant";
            this.etudiantBindingSource1.DataSource = this.p2022_Appli_AdministrationDataSet6;
            // 
            // p2022_Appli_AdministrationDataSet6
            // 
            this.p2022_Appli_AdministrationDataSet6.DataSetName = "P2022_Appli_AdministrationDataSet6";
            this.p2022_Appli_AdministrationDataSet6.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // etudiantBindingSource
            // 
            this.etudiantBindingSource.DataMember = "etudiant";
            this.etudiantBindingSource.DataSource = this.p2022_Appli_AdministrationDataSet;
            // 
            // p2022_Appli_AdministrationDataSet
            // 
            this.p2022_Appli_AdministrationDataSet.DataSetName = "P2022_Appli_AdministrationDataSet";
            this.p2022_Appli_AdministrationDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // etudiantTableAdapter
            // 
            this.etudiantTableAdapter.ClearBeforeFill = true;
            // 
            // etudiantTableAdapter1
            // 
            this.etudiantTableAdapter1.ClearBeforeFill = true;
            // 
            // etudiantTableAdapter2
            // 
            this.etudiantTableAdapter2.ClearBeforeFill = true;
            // 
            // p2022_Appli_AdministrationDataSet10
            // 
            this.p2022_Appli_AdministrationDataSet10.DataSetName = "P2022_Appli_AdministrationDataSet10";
            this.p2022_Appli_AdministrationDataSet10.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // etudiantBindingSource3
            // 
            this.etudiantBindingSource3.DataMember = "etudiant";
            this.etudiantBindingSource3.DataSource = this.p2022_Appli_AdministrationDataSet10;
            // 
            // etudiantTableAdapter3
            // 
            this.etudiantTableAdapter3.ClearBeforeFill = true;
            // 
            // etudiantBindingSource4
            // 
            this.etudiantBindingSource4.DataMember = "etudiant";
            this.etudiantBindingSource4.DataSource = this.p2022_Appli_AdministrationDataSet11;
            // 
            // p2022_Appli_AdministrationDataSet11
            // 
            this.p2022_Appli_AdministrationDataSet11.DataSetName = "P2022_Appli_AdministrationDataSet11";
            this.p2022_Appli_AdministrationDataSet11.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // etudiantTableAdapter4
            // 
            this.etudiantTableAdapter4.ClearBeforeFill = true;
            // 
            // etudiantTableAdapter5
            // 
            this.etudiantTableAdapter5.ClearBeforeFill = true;
            // 
            // nometudiant
            // 
            this.nometudiant.DataPropertyName = "nometudiant";
            this.nometudiant.HeaderText = "Nom de l\'etudiant";
            this.nometudiant.MinimumWidth = 8;
            this.nometudiant.Name = "nometudiant";
            this.nometudiant.Width = 150;
            // 
            // prenometudiantDataGridViewTextBoxColumn
            // 
            this.prenometudiantDataGridViewTextBoxColumn.DataPropertyName = "prenometudiant";
            this.prenometudiantDataGridViewTextBoxColumn.HeaderText = "Prénom de l\'étudiant";
            this.prenometudiantDataGridViewTextBoxColumn.MinimumWidth = 230;
            this.prenometudiantDataGridViewTextBoxColumn.Name = "prenometudiantDataGridViewTextBoxColumn";
            this.prenometudiantDataGridViewTextBoxColumn.ReadOnly = true;
            this.prenometudiantDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.prenometudiantDataGridViewTextBoxColumn.Width = 230;
            // 
            // idclasseDataGridViewTextBoxColumn
            // 
            this.idclasseDataGridViewTextBoxColumn.DataPropertyName = "idclasse";
            this.idclasseDataGridViewTextBoxColumn.HeaderText = "Libellé de la classe";
            this.idclasseDataGridViewTextBoxColumn.MinimumWidth = 230;
            this.idclasseDataGridViewTextBoxColumn.Name = "idclasseDataGridViewTextBoxColumn";
            this.idclasseDataGridViewTextBoxColumn.ReadOnly = true;
            this.idclasseDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.idclasseDataGridViewTextBoxColumn.Width = 230;
            // 
            // mailetudiant
            // 
            this.mailetudiant.DataPropertyName = "mailetudiant";
            this.mailetudiant.HeaderText = "Mail";
            this.mailetudiant.MinimumWidth = 6;
            this.mailetudiant.Name = "mailetudiant";
            this.mailetudiant.Width = 125;
            // 
            // teletudiant
            // 
            this.teletudiant.DataPropertyName = "teletudiant";
            this.teletudiant.HeaderText = "Tel";
            this.teletudiant.MinimumWidth = 6;
            this.teletudiant.Name = "teletudiant";
            this.teletudiant.Width = 125;
            // 
            // ListeEtudiant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(18F, 39F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1024, 529);
            this.Controls.Add(this.bFermer);
            this.Controls.Add(this.PanelInterieur);
            this.Controls.Add(this.PanelMenu);
            this.Controls.Add(this.PanelQuitter);
            this.Font = new System.Drawing.Font("MV Boli", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.ForeColor = System.Drawing.Color.Blue;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "ListeEtudiant";
            this.Text = "Liste des Etudiants";
            this.Load += new System.EventHandler(this.ListeEtudiant_Load);
            this.PanelQuitter.ResumeLayout(false);
            this.PanelMenu.ResumeLayout(false);
            this.PanelMenu.PerformLayout();
            this.msGlobal.ResumeLayout(false);
            this.msGlobal.PerformLayout();
            this.PanelInterieur.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListeEtudiant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet11)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel PanelQuitter;
        private System.Windows.Forms.Button bQuitter;
        private System.Windows.Forms.Panel PanelMenu;
        private System.Windows.Forms.MenuStrip msGlobal;
        private System.Windows.Forms.ToolStripMenuItem accueilToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem gestionEtudiantToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem listeDesEtudiantsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajouterUnEtudiantToolStripMenuItem;
        private System.Windows.Forms.Panel PanelInterieur;
        private P2022_Appli_AdministrationDataSet p2022_Appli_AdministrationDataSet;
        private System.Windows.Forms.BindingSource etudiantBindingSource;
        private P2022_Appli_AdministrationDataSetTableAdapters.etudiantTableAdapter etudiantTableAdapter;
        private System.Windows.Forms.Button bFermer;
        private System.Windows.Forms.ToolStripMenuItem gestionClasseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesClassesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajouterUneClasseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionCoursToolStripMenuItem;
        private P2022_Appli_AdministrationDataSet6 p2022_Appli_AdministrationDataSet6;
        private System.Windows.Forms.BindingSource etudiantBindingSource1;
        private P2022_Appli_AdministrationDataSet6TableAdapters.etudiantTableAdapter etudiantTableAdapter1;
        private P2022_Appli_AdministrationDataSet8 p2022_Appli_AdministrationDataSet8;
        private System.Windows.Forms.BindingSource etudiantBindingSource2;
        private P2022_Appli_AdministrationDataSet8TableAdapters.etudiantTableAdapter etudiantTableAdapter2;
        private System.Windows.Forms.ToolStripMenuItem listeDesCoursToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajouterUnCoursToolStripMenuItem;
        private P2022_Appli_AdministrationDataSet10 p2022_Appli_AdministrationDataSet10;
        private System.Windows.Forms.BindingSource etudiantBindingSource3;
        private P2022_Appli_AdministrationDataSet10TableAdapters.etudiantTableAdapter etudiantTableAdapter3;
        private P2022_Appli_AdministrationDataSet11 p2022_Appli_AdministrationDataSet11;
        private System.Windows.Forms.BindingSource etudiantBindingSource4;
        private P2022_Appli_AdministrationDataSet11TableAdapters.etudiantTableAdapter etudiantTableAdapter4;
        private P2022_Appli_AdministrationDataSet12 p2022_Appli_AdministrationDataSet12;
        private System.Windows.Forms.BindingSource etudiantBindingSource5;
        private P2022_Appli_AdministrationDataSet12TableAdapters.etudiantTableAdapter etudiantTableAdapter5;
        private System.Windows.Forms.DataGridView dgvListeEtudiant;
        private System.Windows.Forms.DataGridViewTextBoxColumn nometudiant;
        private System.Windows.Forms.DataGridViewTextBoxColumn prenometudiantDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idclasseDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mailetudiant;
        private System.Windows.Forms.DataGridViewTextBoxColumn teletudiant;
    }
}