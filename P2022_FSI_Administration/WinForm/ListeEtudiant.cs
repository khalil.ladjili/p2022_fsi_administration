﻿using P2022_FSI_Administration.Classe;
using P2022_FSI_Administration.WinForm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace P2022_FSI_Administration
{
    public partial class ListeEtudiant : Form
    {
        Utilisateur uti;
        public ListeEtudiant(Utilisateur uticonnecte)
        {
            InitializeComponent();
            uti = uticonnecte;
            Form formAccueil = new Accueil(uti);
            formAccueil.Close();
        }

        private void accueilToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form formAccueil = new Accueil(uti);
            formAccueil.Show();
        }

        private void ajouterUnEtudiantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form formAjouterEtudiant = new AjouterEtudiant();
            formAjouterEtudiant.Show();
        }

        private void listeDesEtudiantsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PanelInterieur.BringToFront();
        }

        private void ListeEtudiant_Load(object sender, EventArgs e)
        {
            // TODO: cette ligne de code charge les données dans la table 'p2022_Appli_AdministrationDataSet12.etudiant'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.etudiantTableAdapter5.Fill(this.p2022_Appli_AdministrationDataSet12.etudiant);
            // TODO: cette ligne de code charge les données dans la table 'p2022_Appli_AdministrationDataSet11.etudiant'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            // this.etudiantTableAdapter4.Fill(this.p2022_Appli_AdministrationDataSet11.etudiant);
            // TODO: cette ligne de code charge les données dans la table 'p2022_Appli_AdministrationDataSet10.etudiant'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            // this.etudiantTableAdapter3.Fill(this.p2022_Appli_AdministrationDataSet10.etudiant);
            // TODO: cette ligne de code charge les données dans la table 'p2022_Appli_AdministrationDataSet10.etudiant'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            // this.etudiantTableAdapter2.Fill(this.p2022_Appli_AdministrationDataSet8.etudiant);

            // TODO: cette ligne de code charge les données dans la table 'p2022_Appli_AdministrationDataSet6.etudiant'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            // this.etudiantTableAdapter1.Fill(this.p2022_Appli_AdministrationDataSet6.etudiant);
            // TODO: cette ligne de code charge les données dans la table 'p2022_Appli_AdministrationDataSet.etudiant'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            // this.etudiantTableAdapter.Fill(this.p2022_Appli_AdministrationDataSet.etudiant);



        }

        private void bQuitter_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bFermer_Click(object sender, EventArgs e)
        {
            Form formAccueil = new Accueil(uti);
            formAccueil.Show();
            this.Close();
        }

        private void dgvListeEtudiant_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void pbLogo_Click(object sender, EventArgs e)
        {

        }

        private void gestionCoursToolStripMenuItem_Click(object sender, EventArgs e)
        {
 
        }

        private void gestionClasseToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void gestionEtudiantToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void listeDesClassesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
