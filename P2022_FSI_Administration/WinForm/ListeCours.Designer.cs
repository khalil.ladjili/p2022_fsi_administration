﻿
namespace P2022_FSI_Administration.WinForm
{
    partial class ListeCours
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gestionCoursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeCoursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ajoutCoursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.etudiantTableAdapter = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSetTableAdapters.etudiantTableAdapter();
            this.p2022_Appli_AdministrationDataSet = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet();
            this.etudiantBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PanelInterieur = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.coursBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.p2022_Appli_AdministrationDataSet13 = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet13();
            this.coursBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.p2022_Appli_AdministrationDataSet5 = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet5();
            this.ajouterUneClasseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesClassesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bFermer = new System.Windows.Forms.Button();
            this.gestionClasseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listeDesEtudiantsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionEtudiantToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ajouterUnEtudiantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accueilToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.msGlobal = new System.Windows.Forms.MenuStrip();
            this.PanelMenu = new System.Windows.Forms.Panel();
            this.bQuitter = new System.Windows.Forms.Button();
            this.PanelQuitter = new System.Windows.Forms.Panel();
            this.coursTableAdapter = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet5TableAdapters.coursTableAdapter();
            this.coursTableAdapter1 = new P2022_FSI_Administration.P2022_Appli_AdministrationDataSet13TableAdapters.coursTableAdapter();
            this.idcoursDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.libellecoursDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptioncoursDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idclasseDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource)).BeginInit();
            this.PanelInterieur.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coursBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coursBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet5)).BeginInit();
            this.msGlobal.SuspendLayout();
            this.PanelMenu.SuspendLayout();
            this.PanelQuitter.SuspendLayout();
            this.SuspendLayout();
            // 
            // gestionCoursToolStripMenuItem
            // 
            this.gestionCoursToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listeCoursToolStripMenuItem,
            this.ajoutCoursToolStripMenuItem});
            this.gestionCoursToolStripMenuItem.Name = "gestionCoursToolStripMenuItem";
            this.gestionCoursToolStripMenuItem.Size = new System.Drawing.Size(139, 29);
            this.gestionCoursToolStripMenuItem.Text = "Gestion Cours";
            // 
            // listeCoursToolStripMenuItem
            // 
            this.listeCoursToolStripMenuItem.Name = "listeCoursToolStripMenuItem";
            this.listeCoursToolStripMenuItem.Size = new System.Drawing.Size(245, 34);
            this.listeCoursToolStripMenuItem.Text = "Liste des cours";
            this.listeCoursToolStripMenuItem.Click += new System.EventHandler(this.listeCoursToolStripMenuItem_Click);
            // 
            // ajoutCoursToolStripMenuItem
            // 
            this.ajoutCoursToolStripMenuItem.Name = "ajoutCoursToolStripMenuItem";
            this.ajoutCoursToolStripMenuItem.Size = new System.Drawing.Size(245, 34);
            this.ajoutCoursToolStripMenuItem.Text = "Ajouter un cours";
            this.ajoutCoursToolStripMenuItem.Click += new System.EventHandler(this.ajoutCoursToolStripMenuItem_Click);
            // 
            // etudiantTableAdapter
            // 
            this.etudiantTableAdapter.ClearBeforeFill = true;
            // 
            // p2022_Appli_AdministrationDataSet
            // 
            this.p2022_Appli_AdministrationDataSet.DataSetName = "P2022_Appli_AdministrationDataSet";
            this.p2022_Appli_AdministrationDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // etudiantBindingSource
            // 
            this.etudiantBindingSource.DataMember = "etudiant";
            this.etudiantBindingSource.DataSource = this.p2022_Appli_AdministrationDataSet;
            // 
            // PanelInterieur
            // 
            this.PanelInterieur.Controls.Add(this.dataGridView1);
            this.PanelInterieur.Location = new System.Drawing.Point(101, 54);
            this.PanelInterieur.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PanelInterieur.Name = "PanelInterieur";
            this.PanelInterieur.Size = new System.Drawing.Size(907, 602);
            this.PanelInterieur.TabIndex = 16;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idcoursDataGridViewTextBoxColumn,
            this.libellecoursDataGridViewTextBoxColumn,
            this.descriptioncoursDataGridViewTextBoxColumn,
            this.idclasseDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.coursBindingSource1;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(903, 468);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // coursBindingSource1
            // 
            this.coursBindingSource1.DataMember = "cours";
            this.coursBindingSource1.DataSource = this.p2022_Appli_AdministrationDataSet13;
            // 
            // p2022_Appli_AdministrationDataSet13
            // 
            this.p2022_Appli_AdministrationDataSet13.DataSetName = "P2022_Appli_AdministrationDataSet13";
            this.p2022_Appli_AdministrationDataSet13.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // coursBindingSource
            // 
            this.coursBindingSource.DataMember = "cours";
            this.coursBindingSource.DataSource = this.p2022_Appli_AdministrationDataSet5;
            // 
            // p2022_Appli_AdministrationDataSet5
            // 
            this.p2022_Appli_AdministrationDataSet5.DataSetName = "P2022_Appli_AdministrationDataSet5";
            this.p2022_Appli_AdministrationDataSet5.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ajouterUneClasseToolStripMenuItem
            // 
            this.ajouterUneClasseToolStripMenuItem.Name = "ajouterUneClasseToolStripMenuItem";
            this.ajouterUneClasseToolStripMenuItem.Size = new System.Drawing.Size(257, 34);
            this.ajouterUneClasseToolStripMenuItem.Text = "Ajouter une classe";
            this.ajouterUneClasseToolStripMenuItem.Click += new System.EventHandler(this.ajouterUneClasseToolStripMenuItem_Click);
            // 
            // listeDesClassesToolStripMenuItem
            // 
            this.listeDesClassesToolStripMenuItem.Name = "listeDesClassesToolStripMenuItem";
            this.listeDesClassesToolStripMenuItem.Size = new System.Drawing.Size(257, 34);
            this.listeDesClassesToolStripMenuItem.Text = "Liste des classes";
            this.listeDesClassesToolStripMenuItem.Click += new System.EventHandler(this.listeDesClassesToolStripMenuItem_Click);
            // 
            // bFermer
            // 
            this.bFermer.BackColor = System.Drawing.Color.LightCyan;
            this.bFermer.Location = new System.Drawing.Point(1015, 75);
            this.bFermer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.bFermer.Name = "bFermer";
            this.bFermer.Size = new System.Drawing.Size(129, 50);
            this.bFermer.TabIndex = 18;
            this.bFermer.Text = "FERMER";
            this.bFermer.UseVisualStyleBackColor = false;
            this.bFermer.Click += new System.EventHandler(this.bFermer_Click);
            // 
            // gestionClasseToolStripMenuItem
            // 
            this.gestionClasseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listeDesClassesToolStripMenuItem,
            this.ajouterUneClasseToolStripMenuItem});
            this.gestionClasseToolStripMenuItem.Name = "gestionClasseToolStripMenuItem";
            this.gestionClasseToolStripMenuItem.Size = new System.Drawing.Size(139, 29);
            this.gestionClasseToolStripMenuItem.Text = "Gestion classe";
            this.gestionClasseToolStripMenuItem.Click += new System.EventHandler(this.gestionClasseToolStripMenuItem_Click);
            // 
            // listeDesEtudiantsToolStripMenuItem
            // 
            this.listeDesEtudiantsToolStripMenuItem.Name = "listeDesEtudiantsToolStripMenuItem";
            this.listeDesEtudiantsToolStripMenuItem.Size = new System.Drawing.Size(267, 34);
            this.listeDesEtudiantsToolStripMenuItem.Text = "Liste des étudiants";
            this.listeDesEtudiantsToolStripMenuItem.Click += new System.EventHandler(this.listeDesEtudiantsToolStripMenuItem_Click);
            // 
            // gestionEtudiantToolStripMenuItem1
            // 
            this.gestionEtudiantToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listeDesEtudiantsToolStripMenuItem,
            this.ajouterUnEtudiantToolStripMenuItem});
            this.gestionEtudiantToolStripMenuItem1.Name = "gestionEtudiantToolStripMenuItem1";
            this.gestionEtudiantToolStripMenuItem1.Size = new System.Drawing.Size(158, 29);
            this.gestionEtudiantToolStripMenuItem1.Text = "Gestion Etudiant";
            // 
            // ajouterUnEtudiantToolStripMenuItem
            // 
            this.ajouterUnEtudiantToolStripMenuItem.Name = "ajouterUnEtudiantToolStripMenuItem";
            this.ajouterUnEtudiantToolStripMenuItem.Size = new System.Drawing.Size(267, 34);
            this.ajouterUnEtudiantToolStripMenuItem.Text = "Ajouter un étudiant";
            this.ajouterUnEtudiantToolStripMenuItem.Click += new System.EventHandler(this.ajouterUnEtudiantToolStripMenuItem_Click);
            // 
            // accueilToolStripMenuItem2
            // 
            this.accueilToolStripMenuItem2.Name = "accueilToolStripMenuItem2";
            this.accueilToolStripMenuItem2.Size = new System.Drawing.Size(83, 29);
            this.accueilToolStripMenuItem2.Text = "Accueil";
            this.accueilToolStripMenuItem2.Click += new System.EventHandler(this.accueilToolStripMenuItem2_Click_1);
            // 
            // msGlobal
            // 
            this.msGlobal.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.msGlobal.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.msGlobal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accueilToolStripMenuItem2,
            this.gestionEtudiantToolStripMenuItem1,
            this.gestionClasseToolStripMenuItem,
            this.gestionCoursToolStripMenuItem});
            this.msGlobal.Location = new System.Drawing.Point(0, 0);
            this.msGlobal.Name = "msGlobal";
            this.msGlobal.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
            this.msGlobal.Size = new System.Drawing.Size(907, 33);
            this.msGlobal.TabIndex = 0;
            this.msGlobal.Text = "Menu";
            // 
            // PanelMenu
            // 
            this.PanelMenu.Controls.Add(this.msGlobal);
            this.PanelMenu.Location = new System.Drawing.Point(101, 10);
            this.PanelMenu.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PanelMenu.Name = "PanelMenu";
            this.PanelMenu.Size = new System.Drawing.Size(907, 36);
            this.PanelMenu.TabIndex = 17;
            // 
            // bQuitter
            // 
            this.bQuitter.BackColor = System.Drawing.Color.LightCyan;
            this.bQuitter.Location = new System.Drawing.Point(4, 4);
            this.bQuitter.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.bQuitter.Name = "bQuitter";
            this.bQuitter.Size = new System.Drawing.Size(129, 50);
            this.bQuitter.TabIndex = 7;
            this.bQuitter.Text = "QUITTER";
            this.bQuitter.UseVisualStyleBackColor = false;
            this.bQuitter.Click += new System.EventHandler(this.bQuitter_Click);
            // 
            // PanelQuitter
            // 
            this.PanelQuitter.Controls.Add(this.bQuitter);
            this.PanelQuitter.Location = new System.Drawing.Point(1015, 10);
            this.PanelQuitter.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PanelQuitter.Name = "PanelQuitter";
            this.PanelQuitter.Size = new System.Drawing.Size(138, 58);
            this.PanelQuitter.TabIndex = 15;
            // 
            // coursTableAdapter
            // 
            this.coursTableAdapter.ClearBeforeFill = true;
            // 
            // coursTableAdapter1
            // 
            this.coursTableAdapter1.ClearBeforeFill = true;
            // 
            // idcoursDataGridViewTextBoxColumn
            // 
            this.idcoursDataGridViewTextBoxColumn.DataPropertyName = "idcours";
            this.idcoursDataGridViewTextBoxColumn.HeaderText = "Numero du cours";
            this.idcoursDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.idcoursDataGridViewTextBoxColumn.Name = "idcoursDataGridViewTextBoxColumn";
            this.idcoursDataGridViewTextBoxColumn.Width = 125;
            // 
            // libellecoursDataGridViewTextBoxColumn
            // 
            this.libellecoursDataGridViewTextBoxColumn.DataPropertyName = "libellecours";
            this.libellecoursDataGridViewTextBoxColumn.HeaderText = "Nom du cours";
            this.libellecoursDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.libellecoursDataGridViewTextBoxColumn.Name = "libellecoursDataGridViewTextBoxColumn";
            this.libellecoursDataGridViewTextBoxColumn.Width = 125;
            // 
            // descriptioncoursDataGridViewTextBoxColumn
            // 
            this.descriptioncoursDataGridViewTextBoxColumn.DataPropertyName = "descriptioncours";
            this.descriptioncoursDataGridViewTextBoxColumn.HeaderText = "Descripton du cours";
            this.descriptioncoursDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.descriptioncoursDataGridViewTextBoxColumn.Name = "descriptioncoursDataGridViewTextBoxColumn";
            this.descriptioncoursDataGridViewTextBoxColumn.Width = 125;
            // 
            // idclasseDataGridViewTextBoxColumn
            // 
            this.idclasseDataGridViewTextBoxColumn.DataPropertyName = "idclasse";
            this.idclasseDataGridViewTextBoxColumn.HeaderText = "Classe";
            this.idclasseDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.idclasseDataGridViewTextBoxColumn.Name = "idclasseDataGridViewTextBoxColumn";
            this.idclasseDataGridViewTextBoxColumn.Width = 125;
            // 
            // ListeCours
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1156, 666);
            this.Controls.Add(this.PanelInterieur);
            this.Controls.Add(this.bFermer);
            this.Controls.Add(this.PanelMenu);
            this.Controls.Add(this.PanelQuitter);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ListeCours";
            this.Text = "ListeCours";
            this.Load += new System.EventHandler(this.ListeCours_Load);
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.etudiantBindingSource)).EndInit();
            this.PanelInterieur.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coursBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coursBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.p2022_Appli_AdministrationDataSet5)).EndInit();
            this.msGlobal.ResumeLayout(false);
            this.msGlobal.PerformLayout();
            this.PanelMenu.ResumeLayout(false);
            this.PanelMenu.PerformLayout();
            this.PanelQuitter.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem gestionCoursToolStripMenuItem;
        private P2022_Appli_AdministrationDataSetTableAdapters.etudiantTableAdapter etudiantTableAdapter;
        private P2022_Appli_AdministrationDataSet p2022_Appli_AdministrationDataSet;
        private System.Windows.Forms.BindingSource etudiantBindingSource;
        private System.Windows.Forms.Panel PanelInterieur;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ToolStripMenuItem ajouterUneClasseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesClassesToolStripMenuItem;
        private System.Windows.Forms.Button bFermer;
        private System.Windows.Forms.ToolStripMenuItem gestionClasseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesEtudiantsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionEtudiantToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ajouterUnEtudiantToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accueilToolStripMenuItem2;
        private System.Windows.Forms.MenuStrip msGlobal;
        private System.Windows.Forms.Panel PanelMenu;
        private System.Windows.Forms.Button bQuitter;
        private System.Windows.Forms.Panel PanelQuitter;
        private P2022_Appli_AdministrationDataSet5 p2022_Appli_AdministrationDataSet5;
        private System.Windows.Forms.BindingSource coursBindingSource;
        private P2022_Appli_AdministrationDataSet5TableAdapters.coursTableAdapter coursTableAdapter;
        private System.Windows.Forms.ToolStripMenuItem listeCoursToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajoutCoursToolStripMenuItem;
        private P2022_Appli_AdministrationDataSet13 p2022_Appli_AdministrationDataSet13;
        private System.Windows.Forms.BindingSource coursBindingSource1;
        private P2022_Appli_AdministrationDataSet13TableAdapters.coursTableAdapter coursTableAdapter1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idcoursDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn libellecoursDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptioncoursDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idclasseDataGridViewTextBoxColumn;
    }
}