﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P2022_FSI_Administration.Classe
{
    public class Classe
    {
        private int IdClasse { get; set; }
        private string LibelleClasse { get; set; }
        private Cours MesCours { get; set; }
        private Etudiant[] MesEtudiants { get; set; }



        public Classe(string libelle, int classe, Cours mesCours, Etudiant[] mesEtudiants)
        {
            this.LibelleClasse = libelle;
            this.IdClasse = classe;
            this.MesCours = mesCours;
            this.MesEtudiants = mesEtudiants;
        }
    }
}
