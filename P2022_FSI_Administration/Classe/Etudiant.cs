﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P2022_FSI_Administration.Classe
{
    public class Etudiant
    {
        private int idEtudiant;
        private string nomEtudiant;
        private string prenomEtudiant;
        private Classe maClasse;

        public Etudiant(string nom, string prenom, string mailEtu, string telEtu, Classe maClasse)
        {
            this.nomEtudiant = nom;
            this.prenomEtudiant = prenom;
            this.maClasse = maClasse;
            this.mailEtudiant = mailEtu;
            this.telEtudiant = telEtu;

        }

        public int IdEtudiant { get => idEtudiant; set => idEtudiant = value; }
        public string NomEtudiant { get => nomEtudiant; set => nomEtudiant = value; }
        public string PrenomEtudiant { get => prenomEtudiant; set => prenomEtudiant = value; }
        public string mailEtudiant { get => mailEtudiant; set => mailEtudiant = value; }
        public string telEtudiant { get => telEtudiant; set => telEtudiant = value; }
    }
}
