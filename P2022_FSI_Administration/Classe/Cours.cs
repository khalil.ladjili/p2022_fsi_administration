﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P2022_FSI_Administration.Classe
{
    public class Cours
    {
        public Classe MaClasse { get; set; }
        public int idCours { get; set; }
        public string libelleCours { get; set; }
        public string DescriptionCours { get; set; }

        public Cours(string libelle, string description, Classe maClasse)
        {
            this.libelleCours = libelle;
            this.DescriptionCours = description;
            this.MaClasse = maClasse;
        }
    }
}
